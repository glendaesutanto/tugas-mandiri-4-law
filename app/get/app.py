import os
from typing import Optional
from fastapi import FastAPI, File, UploadFile, status, HTTPException
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder
from pydantic import BaseModel, Field
from bson import ObjectId
from bson.binary import Binary
import motor.motor_asyncio

app = FastAPI()
MONGODB_PASSWORD=""
MONGODB_URL=f"mongodb+srv://glendaesutanto:{MONGODB_PASSWORD}@mongodbcluster.2soxp.mongodb.net/test?retryWrites=true&w=majority&tls=true&tlsAllowInvalidCertificates=true"
client = motor.motor_asyncio.AsyncIOMotorClient(MONGODB_URL)
db = client.tm_4

class PyObjectId(ObjectId):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, v):
        if not ObjectId.is_valid(v):
            raise ValueError("Invalid objectid")
        return ObjectId(v)

    @classmethod
    def __modify_schema__(cls, field_schema):
        field_schema.update(type="string")


class Student(BaseModel):
    id: PyObjectId = Field(default_factory=PyObjectId, alias="_id")
    npm: str
    name: str

    class Config:
        allow_population_by_field_name = True
        arbitrary_types_allowed = True
        json_encoders = {ObjectId: str}
        schema_extra = {
            "example": {
                "npm": "1806133774",
                "name": "Glenda Emanuella Sutanto",
            }
        }


@app.get("/read/{npm}", response_description="Get student by npm non-idempotent")
async def get_student_by_npm_non_idempotent(npm: str):
    if (student := await db["students_tm4"].find_one({"npm": npm})) is not None:
        return JSONResponse(status_code=status.HTTP_200_OK, content={'status': 'OK', 'npm': student['npm'], 'nama': student['name']})

    raise HTTPException(status_code=404, detail=f"Student with npm {npm} not found")


@app.get("/read/{npm}/{id}", response_description="Get student by npm idempotent")
async def get_student_by_npm_idempotent(npm: str, id: str):
    if (student := await db["students_tm4"].find_one({"npm": npm})) is not None:
        print(student)
        print(type(student))
        return JSONResponse(status_code=status.HTTP_200_OK, content={'status': 'OK', 'npm': student['npm'], 'nama': student['name']})

    raise HTTPException(status_code=404, detail=f"Student with npm {npm} not found")
