import random
from locust import HttpUser, task, between

class Test(HttpUser):

    @task
    def test_get_non_idempotent(self):
        self.client.get("/read/1806133774")

    @task
    def test_get_idempotent(self):
        self.client.get(f"/read/1806133774/1306")

